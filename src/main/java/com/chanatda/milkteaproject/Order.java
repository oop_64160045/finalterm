/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanatda.milkteaproject;

/**
 *
 * @author chana
 */
public class Order {
 private String nickname;
 private String size;
 private String menu;
 private String style;
 private String sugarlevel;
 private String toppings;
 private String note;      

    @Override
    public String toString() {
        return "Order {" + " Nickname : " + nickname + " , Size : " + size + " , Menu : " + menu + " , Style : " + style + " , SugarLevel : " + sugarlevel + " , Toppings : " + toppings + " , Note : " + note + '}';
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getSugarlevel() {
        return sugarlevel;
    }

    public void setSugarlevel(String sugarlevel) {
        this.sugarlevel = sugarlevel;
    }

    public String getToppings() {
        return toppings;
    }

    public void setToppings(String toppings) {
        this.toppings = toppings;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Order(String nickname, String size, String menu, String style, String sugarlevel, String toppings, String note) {
        this.nickname = nickname;
        this.size = size;
        this.menu = menu;
        this.style = style;
        this.sugarlevel = sugarlevel;
        this.toppings = toppings;
        this.note = note;
    }
}
